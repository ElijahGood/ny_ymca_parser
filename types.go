package main

type Coordinates struct {
	Latitude  float32
	Longitude float32
}

type Member struct {
	Name     string
	Position string
	Email    string
	Phone    string
}

type Branch struct {
	Name     string
	Address  string
	Phone    string
	Location Coordinates
	Url      string
	Staff    []Member
}

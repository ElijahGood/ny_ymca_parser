package main

import (
	"context"
	"log"
	"strings"
	"sync"

	colly "github.com/gocolly/colly/v2"
	"googlemaps.github.io/maps"
)

func ParseMemberItemByP(elem *colly.HTMLElement, members chan<- Member) {
	domHtml, err := elem.DOM.Html()
	if err != nil {
		return
	}
	domHtml = strings.TrimSuffix(strings.TrimRight(strings.TrimPrefix(domHtml, `<br/>`), "\u00a0"), `<br/>`)
	splitted := strings.Split(strings.Replace(strings.Replace(domHtml, "\n", "", -1), "\u00a0", "", -1), `<br/>`)
	if len(splitted) < 2 {
		return
	}
	name := strings.TrimSpace(strings.Trim(strings.Trim(splitted[0], "<strong></strong>"), "<b></b>"))
	if name == "" {
		name := elem.ChildText(`strong`)
		if name == "" {
			name = elem.ChildText(`b`)
			if name == "" {
				return
			}
		}
	}
	mail := strings.TrimPrefix(elem.ChildAttr(`a[href]`, "href"), "mailto:")
	position := strings.Trim(strings.Replace(splitted[1], `&amp;`, `&`, -1), "<strong></strong>")
	if strings.Contains(position, "<span") {
		position = position[strings.Index(position, ">")+1:]
	}
	phone := ""
	if len(splitted) > 3 && splitted[3] != "" {
		phone = splitted[3]
		if strings.Index(phone, "@") != -1 && len(splitted) > 4 {
			phone = splitted[4]
			//mb concat position with splitted[2]
		}
	}

	newMember := Member{
		Name:     name,
		Position: position,
		Email:    mail,
		Phone:    phone,
	}
	members <- newMember
}

func ParseMemberItemForMcBurney(e *colly.HTMLElement, members chan<- Member) {
	var mcArray []string
	e.ForEach("p", func(_ int, elem *colly.HTMLElement) {
		domText := strings.TrimSpace(elem.DOM.Text())
		if domText != "\u00a0" {
			mcArray = append(mcArray, domText)
		}
	})
	for i, item := range mcArray {
		if strings.Index(item, "@") != -1 {
			newMember := Member{
				Name:     mcArray[i-2],
				Position: mcArray[i-1],
				Email:    item,
				Phone:    "",
			}
			members <- newMember
		}
	}
}

func AppendMember(e *colly.HTMLElement, members chan<- Member) {
	// fmt.Printf(" 	from Append: %s", e.Response.Ctx.Get("BranchName"))
	if strings.Index(e.Response.Ctx.Get("BranchName"), `McBurney`) != -1 {
		ParseMemberItemForMcBurney(e, members)
	} else {
		e.ForEach("p", func(_ int, elem *colly.HTMLElement) {
			ParseMemberItemByP(elem, members)
		})
	}
}

func GetMembersList(url, title string) []Member {

	var branch_wg sync.WaitGroup
	collector := colly.NewCollector()
	members := make(chan Member)
	var staff []Member

	collector.OnRequest(func(r *colly.Request) {
		r.Ctx.Put("BranchName", title)
	})
	collector.OnError(func(_ *colly.Response, err error) {
		log.Println("Something went wrong:", err)
	})

	collector.OnHTML(`.field-prgf-description.field-item`, func(e *colly.HTMLElement) {
		branch_wg.Add(1)
		go func(e *colly.HTMLElement, wg *sync.WaitGroup, members chan<- Member) {
			defer branch_wg.Done()
			h3 := e.ChildText(`h3`)
			h2 := e.ChildText(`h2`)
			if strings.Contains(h3, "Staff") || strings.Contains(h2, "Staff") {
				AppendMember(e, members)
			} else if strings.Contains(h3, "Leadership") || strings.Contains(h2, "Leadership") {
				AppendMember(e, members)
			}
		}(e, &branch_wg, members)
	})

	collector.OnHTML(`.field-prgf-2c-left.block-description--text`, func(e *colly.HTMLElement) {
		branch_wg.Add(1)
		go func(e *colly.HTMLElement, wg *sync.WaitGroup, members chan<- Member) {
			defer branch_wg.Done()
			h4 := e.ChildText(`h4`)
			if strings.Contains(h4, "Staff") {
				AppendMember(e, members)
			}
		}(e, &branch_wg, members)
	})

	url += "/about"
	collector.Visit(url)

	go func() {
		branch_wg.Wait()
		close(members)
	}()

	for member := range members {
		staff = append(staff, member)
	}

	return staff
}

func ParseBranchInfo(e *colly.HTMLElement) Branch {
	phone := e.ChildText(`div[class=wrapper-field-location-phone]`)
	address := e.ChildText(`div[class=field-location-direction]`)
	title := e.ChildText(`h2`)
	branchInfoLink := e.Request.AbsoluteURL(e.ChildAttr(`a[class=btn-primary]`, "href"))

	/* Coordinates retrieving */
	r := &maps.GeocodingRequest{
		Address: address,
	}
	geocode, err := GEOCODER.Geocode(context.Background(), r)
	if err != nil {
		log.Fatalf("fatal error: %s", err)
	}
	location := Coordinates{
		Latitude:  float32(geocode[0].Geometry.Location.Lat),
		Longitude: float32(geocode[0].Geometry.Location.Lng),
	}

	/* Fake coordinates */
	// location := Coordinates{
	// 	Latitude:  42,
	// 	Longitude: 42,
	// }

	staff := GetMembersList(branchInfoLink, title)

	branch := Branch{
		Name:     title,
		Address:  address,
		Phone:    phone,
		Location: location,
		Url:      branchInfoLink,
		Staff:    staff,
	}

	return branch
}

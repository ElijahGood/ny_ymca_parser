module gitlab.com/ElijahGood/ny_ymca_parser

go 1.13

require (
	github.com/gocolly/colly/v2 v2.1.0
	googlemaps.github.io/maps v1.3.2 // indirect
)

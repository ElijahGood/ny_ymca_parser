package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"sync"

	colly "github.com/gocolly/colly/v2"
	"googlemaps.github.io/maps"
)

var API_KEY string
var GEOCODER *maps.Client

func init() {
	key, err := ioutil.ReadFile("./google_api_key")
	if err != nil {
		panic(err)
	} else {
		API_KEY = string(key)
	}
	geocoder, err := maps.NewClient(maps.WithAPIKey(API_KEY))
	if err != nil {
		log.Fatalf("fatal error: %s", err)
	}
	GEOCODER = geocoder
}

func main() {
	c := colly.NewCollector()
	branches := make(chan Branch)
	var wg sync.WaitGroup
	var result []Branch

	c.OnHTML(`div[class=location-list-item]`, func(e *colly.HTMLElement) {
		wg.Add(1)
		go func(e *colly.HTMLElement, wg *sync.WaitGroup) {
			defer wg.Done()
			branch := ParseBranchInfo(e)
			branches <- branch
		}(e, &wg)
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})

	c.Visit("https://ymcanyc.org/locations?type&amenities")

	go func() {
		wg.Wait()
		close(branches)
	}()

	for branch := range branches {
		result = append(result, branch)
	}
}
